package tm.client;

import tm.util.Packet;
import tm.util.Util;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * Classe représentant le client du point de vue réseau.
 * Ses fonctions sont appelées lors de la connexion ou lors de la réception / de l'envoi de paquets de données sur le réseau.
 */
public class TMClient {
    private Socket socket = null; // socket contenant la connexion réseau une fois celle-ci effectuée
    private final String addr, code; // adresse et code tels que donnés par l'utilisateur

    public TMClient(String addr, String code) {
        this.addr = addr;
        this.code = code;
    }

    /**
     * Tente de se connecter au serveur (ordinateur) avec les données entrées par l'utilisateur.
     *
     * @return true si la connection réussit, false si elle échoue
     */
    public boolean connect() {
        try {
            this.socket = new Socket(addr, 8424);
            InputStream inputStream = this.socket.getInputStream();
            OutputStream outputStream = this.socket.getOutputStream();
            Util.sendPacket(new Packet(Packet.Packets.WELCOME.ID, ByteBuffer.wrap(this.code.getBytes(StandardCharsets.UTF_8))), inputStream, outputStream);
            Packet response = Util.receivePacket(inputStream);
            if (response == null || response.getID() != Packet.Packets.WELCOME.RESPONSE_ID || !new String(response.getData().array(), StandardCharsets.UTF_8).equals("hi"))
                return false;
            System.out.println(Util.sendPacket(new Packet(Packet.Packets.LS.ID, ByteBuffer.wrap("/home/gaston/bin/backup-tmp".getBytes(StandardCharsets.UTF_8))), inputStream, outputStream));
            System.out.println(new String(Util.receivePacket(inputStream).getData().array(), StandardCharsets.UTF_8));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Envoie le paquet "packet" au serveur.
     * (existe car on ne peut pas utiliser Util.sendPacket(packet, inputStream, outputStream) depuis l'extérieur de cette classe.
     *  En effet, les flux réseaux this.socket.getInputStream() et this.socket.getOutputStream() sont privés et accessibles uniquement au sein de TMClient.)
     *
     * @param packet paquet devant être envoyé
     * @return true si le paquet est bien réceptionné de l'autre côté, false sinon
     */
    public boolean sendPacket(Packet packet) {
        if (this.socket != null) {
            try {
                Util.sendPacket(packet, this.socket.getInputStream(), this.socket.getOutputStream());

                return true;
            } catch (IOException e) {
                e.printStackTrace();

                return false;
            }
        }
        return false;
    }

    /**
     * Reçoit un paquet envoyé par le serveur. Bloque le thread tant qu'aucun paquet n'est reçu.
     * (voir sendPacket(packet) pour le commentaire quant à l'utilité de la fonction par rapport à Util.receivePacket(inputStream))
     *
     * @return paquet reçu; null en cas d'erreur
     */
    public Packet receivePacket() {
        if (this.socket != null) {
            try {
                return Util.receivePacket(this.socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Ferme la connexion si elle existe.
     *
     * @throws IOException
     */
    public void closeAll() throws IOException {
        if (this.socket != null)
            this.socket.close();
    }
}
