//voir https://developer.android.com/training/sharing/receive#java
package tm.client;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import org.w3c.dom.Text;
import tm.util.Util;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Écran qui apparaît lorsqu'on sélectionne mon application dans le menu "partager" dans d'autres apps.
 * Depuis cet écran, l'utilisateur peut:
 *      - déterminer certains paramètres relatifs à l'intégration dans le document de destination (dimensions, légende, autres paramètres).
 *      - choisir le chemin du document de destination sur l'ordinateur
 *      - choisir le nom sous lequel sera enregistrée la ressource sur l'ordinateur
 *      - finalement, déclencher l'envoi du tout.
 */
public class ShareActivity extends AppCompatActivity {

    public static int imageSettings = 0;

    // Handler: permet aux autres partie de mon app d'envoyer des informations / messages à cet écran.
    public static Handler handler = null;

    // chemin du document tex de destination
    public static String filePath = null;

    public static final int FILE_CHOSEN = 1;

    /**
     * Appelée lors de l'ouverture de cet écran
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("=== onCreate() ===");
        setContentView(R.layout.activity_share);

        imageSettings = 0;

        refreshFields();

        handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.what == FILE_CHOSEN) {
                    ShareActivity.filePath = FolderPickerActivity.pickedFile;
                    System.out.println("Choisi: " + ShareActivity.filePath);
                }
            }
        };

        /*
         * Comme il faut un nouveau OnClickListener pour chaque bouton, on est obligé de faire 3 fois la même chose.
         * Rend les boutons de choix et le texte de la question invisibles une fois le choix effectué.
         * On utilise pas de lambda pour la compatibilité java version <= 7
         */
        // si l'utilisateur choisit de donner une taille fixe
        ((MaterialButton) findViewById(R.id.fixed_size_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareActivity.imageSettings = R.id.fixed_size_button;

                refreshFields();
            }
        });

        // si l'utilisateur veut laisser latex&co faire en ce qui concerne la taille
        ((MaterialButton) findViewById(R.id.auto_size_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareActivity.imageSettings = R.id.auto_size_button;

                refreshFields();
            }
        });

        // pour ouvrir le sélecteur de fichier (pour savoir dans quel .tex il faut ajouter la ressource)
        ((MaterialButton) findViewById(R.id.open_folder_picker_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent folderPickerActivityIntent = new Intent(view.getContext(), FolderPickerActivity.class);
                startActivity(folderPickerActivityIntent);
            }
        });

        // pour déclencher le processus d'envoi vers l'ordinateur
        ((MaterialButton) findViewById(R.id.send_resource)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // on récupère l'adresse à partir de laquelle on peut accéder à l'image partagée
                Uri uri = (Uri) getIntent().getParcelableExtra(Intent.EXTRA_STREAM);

                //System.out.println(uri.getPath());

                InputStream inputStream = null;
                byte[] resourceData = null;

                try {
                    inputStream = getContentResolver().openInputStream(uri); // on doit utiliser une fonction interne à Android pour obtenir un flux pour lire les données de l'image
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

                    // inspiré de https://stackoverflow.com/a/6341123 (auteur: "Kaj")
                    /*
                     * On utilise un ByteArrayOuputStream. Cette classe est en quelque sorte un "faux" flux de sortie, dans le sens qu'en y écrivant, on n'écrit pas véritablement vers un fichier ou vers le réseau, mais vers un array interne servant de tampon.
                     * Une fois qu'on a écrit toutes les données nécessaires à ce tampon (avec byteArrayOutputSteram.write()), on peur récupérer le contenu en un morceau avec toByteArray()
                     */
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int readBytes = 0;
                    while ((readBytes = inputStream.read(buffer)) != -1) { // on lit jusqu'à la fin du flux (quand read() retourne -1, alors qu'en temps normal read() retourne le nombre d'octets lus)
                        byteArrayOutputStream.write(buffer, 0, readBytes);
                    }
                    resourceData = byteArrayOutputStream.toByteArray();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // on utilise la classe ByteArrayLockingWrapper, qui permet de s'assurer que les données ne sont pas écrasées pendant leur traitement (voir classe ByteArrayLockingWrapper)
                ClientService.resourceData.setData(resourceData);
                ClientService.resourceData.lock();

                // on dit au service qu'il faut qu'il envoie l'image, en lui transmettant par l'occasion les paramètres données par l'utilisateur (on n'a pas besoin de transmettre l'image en elle-même, qui est déjà contenue dans ClientService.resourceData, auquel le service peut déjà accéder)
                startService(new Intent(view.getContext(), ClientService.class).putExtra("transmit", new String[]{
                        filePath,
                        ((TextInputEditText) findViewById(R.id.res_name)).getText().toString(),
                        ((TextInputEditText) findViewById(R.id.res_caption)).getText().toString(),
                        ((TextInputEditText) findViewById(R.id.ig_parameters)).getText().toString(),
                        Util.nullIfEmpty(((TextInputEditText) findViewById(R.id.centimeters_width)).getText().toString()),
                        Util.nullIfEmpty(((TextInputEditText) findViewById(R.id.centimeters_height)).getText().toString())
                }));
                finish(); // une fois l'ordre d'envoi lancé, on ferme l'écran
            }
        });
    }

    /**
     * Est exécuté à chaque fois que ShareActivity vient au premier plan
     * ATTENTION: bien distinguer de onCreate() qui n'arrive qu'à la *création* de l'Activity, mais pas si on part et revient sur la même.
     *
     * Concrètement dans mon cas: vérifie si un nouveau chemin de fichier est disponible dans FolderPickerActivity.pickedFile.
     * (ça se passe ici car quand l'utilisateur a fini son choix dans l'explorateur de fichier, cet écran revient au premier plan et onResume() est appelée par Android)
     * Si oui, on le récupère et on l'affiche à l'utilisateur.
     */
    @Override
    protected void onResume() {
        super.onResume();

        /*System.out.println("onResume() ShareActivity");
        System.out.println(FolderPickerActivity.pickedFile);
        System.out.println(filePath);*/

        if (FolderPickerActivity.pickedFile != null) {
            filePath = FolderPickerActivity.pickedFile;

            ((TextView) findViewById(R.id.destination_path)).setText(filePath);

            //System.out.println("Chemin mis à jour");
        }
    }

    /**
     * Met à jour les éléments de l'écran par rapport aux données contenues dans les variables.
     *
     * Fonction appelée à chaque fois qu'un changement susceptible de modifier la disposition de l'écran intervient.
     * Par exemple: quand l'utilisateur choisit l'option "taille fixe", on supprime les boutons et on affiche les cases utilisées pour donner les dimensions.
     */
    public void refreshFields() {
        ((TextView) findViewById(R.id.scaling_options_info)).setText(imageSettings == 0 ? "Quelle mise en page souhaitez-vous?"
                : imageSettings == R.id.fixed_size_button ? "Taille fixe (unité: mm)"
                : imageSettings == R.id.auto_size_button ? "Taille automatique"
                : "autre");

        ((MaterialButton) findViewById(R.id.fixed_size_button)).setVisibility(imageSettings != 0 ? View.INVISIBLE : View.VISIBLE);
        ((MaterialButton) findViewById(R.id.auto_size_button)).setVisibility(imageSettings != 0 ? View.INVISIBLE : View.VISIBLE);

        ((TextInputLayout) findViewById(R.id.centimeters_width_layout)).setVisibility(imageSettings == R.id.fixed_size_button ? View.VISIBLE : View.INVISIBLE);
        ((TextInputLayout) findViewById(R.id.centimeters_height_layout)).setVisibility(imageSettings == R.id.fixed_size_button ? View.VISIBLE : View.INVISIBLE);
        ((TextView) findViewById(R.id.fixed_dimensions_info)).setVisibility(imageSettings == R.id.fixed_size_button ? View.VISIBLE : View.INVISIBLE);
    }
}