package tm.client;

import android.content.Intent;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Classe Adapter nécessaire, servant essentiellement à la gestion des éléments de l'explorateur de fichiers.
 */
public class FolderPickerAdapter extends RecyclerView.Adapter<FolderPickerAdapter.FolderPickerViewHolder> {

    public FolderPickerAdapter() {}

    /**
     * Pour chaque élément (pour chaque fichier / dossier), cette méthode est appelée pour créer une copie d'un élément graphique.
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public FolderPickerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FolderPickerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.textview_folder_picker, parent, false));
    }

    /**
     * Pour chaque élément de la liste, ajoute un nom et une icône (icône dossier ou fichier) à l'élément.
     * À chaque fois, on récupère le nom de l'élément (et en déduit l'icône) dans l'array ClientService.folders (qui contient le résultat de la commande "ls" exécutée sur l'ordinateur)
     *
     * @param holder élément graphique (tel que créé dans onCreateViewHolder())
     * @param position position de l'élément dans la liste
     */
    @Override
    public void onBindViewHolder(@NonNull FolderPickerViewHolder holder, int position) {
        String rawName = ClientService.folders[position]; // nom de l'élément (nom du fichier / dossier)

        // si on trouve un slash à la fin du nom l'élément, c'est un dossier -> on affiche l'icône dossier; sinon on affiche l'icône fichier
        holder.icon.setImageResource(rawName.charAt(rawName.length() - 1) == '/' ? R.drawable.outline_folder_open_black_24 : R.drawable.outline_insert_drive_file_black_24);

        holder.folderName.setText(rawName);

        holder.itemView.setOnClickListener((view) -> { // définition de l'action exécutée si l'utilisateur clique sur un des éléments
            if (rawName.charAt(rawName.length() - 1) == '/' || rawName.equals(".") || rawName.equals("..")) {
                // si c'est un dossier, on demande à l'ordinateur le contenu de ce dossier puis on l'affiche à l'écran (le cycle recommence...)
                view.getContext().startService(new Intent(view.getContext(), ClientService.class).putExtra("cd", ClientService.currentDirectory + "/" + rawName).putExtra("ls", ""));
            } else {
                // sinon (si c'est un fichier), on stocke son chemin puis on envoie à l'écran explorateur de fichiers l'information qu'on a fini le choix et qu'il faut se fermer.
                FolderPickerActivity.pickedFile = ClientService.currentDirectory + "/" + rawName;

                Message.obtain(FolderPickerActivity.handler, FolderPickerActivity.FILE_FOUND).sendToTarget();
            }
        });
    }

    /**
     * Retourne le nombre d'éléments qui doivent être dessinés (nombre de dossiers / fichiers à afficher).
     *
     * @return nombre d'éléments à dessiner
     */
    @Override
    public int getItemCount() {
        return ClientService.folders.length;
    }

    /**
     * Classe décrivant la représentation en Java d'un élément de la liste (du pt de vue graphique).
     * -> contient une référence à l'icône et au nom, qui peuvent ensuite être modifiés à partir de ces références lors de onBindViewHolder()
     */
    public static class FolderPickerViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView folderName;

        public FolderPickerViewHolder(@NonNull View view) {
            super(view);

            this.icon = view.findViewById(R.id.file_folder_icon);
            this.folderName = view.findViewById(R.id.folder_picker_textview);
        }
    }
}
