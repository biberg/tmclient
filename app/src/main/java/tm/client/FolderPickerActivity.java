package tm.client;

import android.content.Intent;
import android.os.*;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Activité (=écran) de l'explorateur de fichiers permettant de choisir le document de destination.
 */
public class FolderPickerActivity extends AppCompatActivity {
    public static final int NEW_LIST = 1;
    public static final int ENOT_CONNECTED = 2;
    public static final int EMISC = 3;
    public static final int FILE_FOUND = 4;

    public static Handler handler = null;

    public static String pickedFile = null;

    /**
     * Appelée au lancement de l'explorateur de fichiers
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder_picker);

        handler = new Handler(Looper.myLooper()) { // permet de recevoir des messages du service
            @Override
            public void handleMessage(@NonNull Message msg) {
                switch (msg.what) {
                    case NEW_LIST:
                        // si on reçoit le message NEW_LIST, on dit à l'adapter qu'il doit refresh les données affichées à l'écran
                        ((RecyclerView) findViewById(R.id.folder_picker)).getAdapter().notifyDataSetChanged();
                        break;
                    case FILE_FOUND:
                        // si on reçoit le message FILE_FOUND, cela signifie que l'utilisateur a cliqué sur un fichier -> on en a fini avec l'explorateur
                        finish(); // on ferme l'explorateur
                    case ENOT_CONNECTED:
                        break;
                }
            }
        };
        ((RecyclerView) findViewById(R.id.folder_picker)).setAdapter(new FolderPickerAdapter()); // on enregistre notre Adapter
        ((RecyclerView) findViewById(R.id.folder_picker)).setLayoutManager(new LinearLayoutManager(this)); // l'explorateur de fichiers doit être géré linéairement (un élément après l'autre tout simplement)
        startService(new Intent(this, ClientService.class).putExtra("ls", "")); // on demande une première fois la liste des fichiers (sans que l'utilisateur ne le demande, pour qu'il ait un point de départ et non un écran vide à partir duquel il ne pourrait rien faire...)
    }
}