package tm.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputEditText;
import tm.util.Util;

/**
 * Activity (écran) de connexion au serveur (à l'ordinateur).
 */
public class MainActivity extends Activity {

    // codes d'erreur possibles
    public static final int CONNECTION_FAILED = 1;
    public static final int CONNECTION_SUCCESSFUL = 2;
    public static final int NOT_CONNECTED = 3;
    public static final int CONNECTED = 4;

    // connexion active ou pas?
    public static boolean connected = false;

    // Handler: permet à toute autre partie de l'application d'envoyer des informations à cet Activity (écran) de connexion.
    public static Handler handler;

    /**
     * Au démarrage de l'écran, on vérifie l'état de la connexion.
     * Si on est connecté, on l'affiche et on cache les cases de connexion.
     * Si on ne l'est pas, on l'affiche et on affiche les cases pour que l'utilisateur puisse entrer les données de connexion et se connecter.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Util.isClient = true; // permet de dire à la classe Util qu'on est le client (change le comportement de certaines fonctions en cas d'erreur, notamment en évitant que l'application se ferme (ce qui causerait un crash))

        setContentView(R.layout.activity_main);

        /*((EditText) findViewById(R.id.ipAddress)).setText("192.168.1.195");
        ((EditText) findViewById(R.id.code)).setText("123456");*/

        if (connected) { // on affiche l'état de connexion; si connecté, on cache les cases et affiche "Connecté"
            findViewById(R.id.ipAddressLayout).setVisibility(View.INVISIBLE);
            findViewById(R.id.codeLayout).setVisibility(View.INVISIBLE);

            ((TextView) findViewById(R.id.status)).setText("Connecté");
        } else { // si on est déconnecté, on affiche "Déconnecté" et affiche les cases des coordonnées de connexion
            findViewById(R.id.ipAddressLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.codeLayout).setVisibility(View.VISIBLE);

            ((TextView) findViewById(R.id.status)).setText("Déconnecté");
        }

        // on crée le Handler pour gérer les messages / informations envoyées par les autres parties de l'application
        handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                switch (msg.what) {
                    case CONNECTION_FAILED: // si la connexion échoue
                        // on réaffiche les cases
                        findViewById(R.id.ipAddressLayout).setVisibility(View.VISIBLE);
                        findViewById(R.id.codeLayout).setVisibility(View.VISIBLE);

                        ((TextView) findViewById(R.id.status)).setText("Connexion échouée!");

                        break;
                    case CONNECTION_SUCCESSFUL: // si la connexion réussit
                        ((TextView) findViewById(R.id.status)).setText("Connexion réussie!");

                        break;
                }
            }
        };

        // permet de n'accepter que les points et les chiffres dans le champ "Adresse IP"
        InputFilter ipInputFilter = (source, start, end, dest, dstart, dend) -> dstart < dend ? null : (dest.toString().substring(0, dstart) + source.subSequence(start, end) + dest.toString().substring(dend)).matches("[1,9]{0,3}(\\.([1,9]{0,3}(\\.([1,9]{0,3}(\\.([1,9]{0,3})?)?)?)?)?)?") ? null : "";
        ((TextInputEditText) findViewById(R.id.ipAddress)).setFilters(new InputFilter[]{ipInputFilter});

        // définit les actions si l'utilisateur clique sur les boutons
        findViewById(R.id.connect).setOnClickListener((view) -> attemptConnection());
        findViewById(R.id.disconnect).setOnClickListener((view) -> disconnect());
    }

    /**
     * Essaye de se connecter au server avec les coordonnées écrites dans les deux cases.
     */
    private void attemptConnection() {
        // on rend invisibles les cases
        findViewById(R.id.ipAddressLayout).setVisibility(View.INVISIBLE);
        findViewById(R.id.codeLayout).setVisibility(View.INVISIBLE);

        ((TextView) findViewById(R.id.status)).setText("Connexion en cours...");

        // on demande au ClientService d'essayer de se connecter en transmettant les données de connexion
        startService(new Intent(this, ClientService.class).putExtra("connect", ((EditText) findViewById(R.id.ipAddress)).getText().toString() + " " + ((EditText) findViewById(R.id.code)).getText().toString()));
    }

    /**
     * Demande au service de se déconnecter
     */
    private void disconnect() {
        startService(new Intent(this, ClientService.class).putExtra("disconnect", ""));

        // on rend visible les cases de connexion pour permettre à l'utilisateur de se reconnecter s'il le souhaite
        findViewById(R.id.ipAddressLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.codeLayout).setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.status)).setText("Déconnecté");
    }
}