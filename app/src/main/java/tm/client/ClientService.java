//voir https://developer.android.com/reference/android/app/Service
package tm.client;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.Message;
import androidx.annotation.Nullable;
import tm.util.ByteArrayLockingWrapper;
import tm.util.Packet;
import tm.util.Util;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class ClientService extends Service {
    // Dossier courant: est utilisé uniquement dans le cadre de l'exploration de l'arbre des fichiers.
    public static String currentDirectory = "/";

    // Dernier résultat reçu du serveur pour ls (listage des fichiers dans currentDirectory)
    public static String[] folders = {"."};

    // là ou va être stocké le tableau d'octets de l'image pendant sa transmission
    public static ByteArrayLockingWrapper resourceData = new ByteArrayLockingWrapper();

    private TMClient tmClient = null;

    /**
     * Cette fonction permet de "nettoyer" les chemins au fur et à mesure de l'exploration.
     * Par exemple, si l'utilisateur revient en arrière plusieurs fois ou ne bouge pas, on se retrouve avec un chemin de ce genre:
     *      /boot/grub/../../media/../home/gaston/././Documents/mon_fichier
     * qui est inutilement long et peu clair, mais qui peut être remplacé par (est équivalent à):
     *      /home/gaston/Documents/mon_fichier
     * (".." signifiant "dossier parent" et "." signifiant "dossier courant")
     *
     * @param path chemin à réduire (nettoyer)
     * @return chemin "irréductible"
     */
    private static String cleanPath(String path) {
        String[] elements = path.split("/"); // sépare le chemin en unités ("/home/gaston/test" devient un tableau {"home", "gaston", "test"})

        /*
         * Ici, il y a besoin d'une boucle "explicite" (avec condition et incrémentation) car on veut aussi explorer les alentours de l'élement central à chaque tour de boucle
         * (impossible avec un foreach, où on aurait seulement accès à l'élément courant)
         */
        StringBuilder cleanPath = new StringBuilder();
        for (int i = 0; i < elements.length; i++) {
            String tmpToAppend = "/"; // on remet le slash au début de chaque élément ajouté

            if (!elements[i].equals("..") && !elements[i].equals(".") && !elements[i].isEmpty()) { // on n'ajoute que si on n'est pas suivi d'un retour en arrière (..) et qu'on sert à qqch (on élimine les "." inutils)
                if ((i + 1) < elements.length) { // vérifie si on n'est pas au dernier élément
                    if (!elements[i + 1].equals("..")) {
                        tmpToAppend += elements[i];
                        cleanPath.append(tmpToAppend);
                    }
                } else { // on en est au dernier élément
                    tmpToAppend += elements[i];
                    cleanPath.append(tmpToAppend);
                }
            }
        }

        if (cleanPath.toString().isEmpty()) { // si on se retrouve avec un chemin vide ça veut dire qu'on est à la racine => il faut mettre un / pour ne pas se retrouver avec un chemin vide
            cleanPath.append("/");
        }

        return cleanPath.toString();
    }

    /*
     * TODO: ClientService: onBind: expliquer pourquoi je n'ai pas besoin de faire d'implémentation
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Appelé une fois lors du lancement initial du service.
     * La seule action à effectuer, dans notre cas, est la création d'une notification "permanente" (qui n'est pas effaçable tant que le service est actif).
     * Cette notification permet d'éviter que notre service (qui tourne en arrière-plan) ne soit tué par le système Android pour des raisons d'économie de mémoire:
     * Quand une notification est présente, le système considère que l'utilisateur "utilise" activement le service et qu'il faut le laisser tranquille
     */
    @Override
    public void onCreate() {
        // Les "notification channels", qui servent principalement à ranger les notifications par importance (faible, importante, urgente, ...), n'existent que depuis Android O
        // On vérifie que la version soit >= à android O, sinon on a une erreur (on ne peut pas utiliser une fonction qui n'existait pas encore...)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).createNotificationChannel(new NotificationChannel("0", "Client Service", NotificationManager.IMPORTANCE_NONE));
            Notification serviceNotification = new Notification.Builder(this, "0")
                    .setSmallIcon(R.drawable.outline_sync_alt_24)
                    .setTicker(getText(R.string.client_connected))
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle("Client")
                    .setContentText("Connecté")
                    .setContentIntent(null)
                    .build();
            startForeground(R.string.client_connected, serviceNotification);
        }
    }

    /**
     * Fonction appelée à chaque instruction qu'on envoie au service, *pas uniquement au démarrage*, comme laisserait penser le nom de la fonction!!!
     *
     * @param intent intent passé au service dans startService()
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(() -> {
            if (intent.getStringExtra("disconnect") != null && this.tmClient != null) {
                try {
                    this.tmClient.closeAll();
                } catch (IOException e) {
                    //on ignore cette exception car peu importe, on veut se déconnecter et c'est tout! tant mieux si la connexion est déjà détruite
                }

                this.tmClient = null;

                stopService(intent);
            }
            if (intent.getStringExtra("connect") != null && this.tmClient == null) {
                connect(intent);
            }
            if (intent.getStringExtra("cd") != null) {
                changeDirectory(intent.getStringExtra("cd"));
            }
            if (intent.getStringExtra("ls") != null && FolderPickerActivity.handler != null) {
                listDirectory();
            }
            if (intent.getStringArrayExtra("transmit") != null && resourceData.getData() != null) {
                //System.out.println("TRANSMISSION!!!!");
                transmitData(intent.getStringArrayExtra("transmit"));
            }
        }).start();

        return START_NOT_STICKY; // si le service est tué, on ne le redémarre pas
    }

    /**
     * Appelé lors de l'arrêt du service.
     * Dans notre cas, on a seulement besoin d'enlever la notification crée précédemment (cf. fonction onStartCommand(...))
     */
    @Override
    public void onDestroy() {
        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancel(R.string.client_connected);

        MainActivity.connected = false;
    }

    /**
     * Tente de se connecter au serveur avec les informations (adresse, code) fournies dans le paramètre "connect"
     * Si ça marche, on le dit à MainActivity pour que l'utilisateur soit informé qu'on a réussi à se connecter
     * Si ça ne marche pas, on s'assure que tous les sockets soient fermés puis on tue le service.
     *
     * @param intent intent reçu par le service (qui contient les données de connexion)
     */
    public void connect(Intent intent) {
        String[] connectionData = intent.getStringExtra("connect").split(" "); // on déballe les données de connexion

        this.tmClient = new TMClient(connectionData[0], connectionData[1]); //0: adresse ip, 1: code

        if (tmClient.connect()) { // si on arrive à se connecter
            Message.obtain(MainActivity.handler, MainActivity.CONNECTION_SUCCESSFUL).sendToTarget();
            MainActivity.connected = true;
        } else { // sinon, on s'assure bien de ne pas laisser de "déchets" derrière soi (que tout est bien fermé et que tmClient reste null)
            Message.obtain(MainActivity.handler, MainActivity.CONNECTION_FAILED).sendToTarget();

            try {
                this.tmClient.closeAll();
            } catch (IOException e) {
            }

            this.tmClient = null;

            stopService(intent);
        }
    }

    /**
     * Change le dossier courant en ce qui concerne l'explorateur de fichiers, en vérifiant qu'il n'y a pas de détours inutils dans le chemin donné
     * (cf. cleanPath())
     *
     * @param rawNewDirectory nouveau dossier courant souhaité
     */
    public void changeDirectory(String rawNewDirectory) {
        currentDirectory = cleanPath(rawNewDirectory);

        System.out.println("Chemin actuel: " + currentDirectory);
    }

    /**
     * Demande au serveur la liste des fichiers/dossiers présents dans le dossier courant (currentDirectory).
     * Stocke le résultat dans une variable, puis notifie FolderPickerActivity que la réponse est arrivée pour que l'écran puisse être rafraîchi avec les nouvelles données.
     */
    public void listDirectory() {
        if (this.tmClient != null) { // on vérifie si on est connecté
            // envoie un paquet LS avec le chemin du dossier courant
            this.tmClient.sendPacket(new Packet(Packet.Packets.LS.ID, ByteBuffer.wrap(currentDirectory.getBytes(StandardCharsets.UTF_8))));

            Packet response = this.tmClient.receivePacket();

            if (response.getID() == Packet.Packets.LS.RESPONSE_ID) {
                folders = new String(response.getData().array(), StandardCharsets.UTF_8).split("\n"); // on sépare tout de suite le résultat en tableau de strings (1 par ligne)
                Message.obtain(FolderPickerActivity.handler, FolderPickerActivity.NEW_LIST).sendToTarget(); // on dit à l'explorateur que du nouveau contenu est disponible et qu'il faut un refresh
            } else {
                Message.obtain(FolderPickerActivity.handler, FolderPickerActivity.EMISC).sendToTarget(); // erreur inconnue
            }
        } else {
            Message.obtain(FolderPickerActivity.handler, FolderPickerActivity.ENOT_CONNECTED).sendToTarget();
        }
    }

    /**
     * Transmet la ressource et les données y relatives (taille, légende, ...).
     *
     * @param resData données relatives à l'image sous forme d'array de string (légende, nom, taille, ...)
     */
    public void transmitData(String[] resData) {
        byte[] stringData = Util.getBytesFromStrings(resData); // octets des PARAMÈTRES concernant l'intégration de l'image
        ByteBuffer stringDataBB = ByteBuffer.wrap(stringData);

        Packet notifyStringData = new Packet(Packet.Packets.TRANSMIT_RES_DATA.ID, stringDataBB);
        this.tmClient.sendPacket(notifyStringData);

        byte[] resourceDataB = resourceData.getData(); // octets de l'IMAGE en elle-même
        ByteBuffer resourceDataBB = ByteBuffer.wrap(resourceDataB);

        Packet sendResourceContent = new Packet(Packet.Packets.TRANSMIT_DATA.ID, resourceDataBB);
        this.tmClient.sendPacket(sendResourceContent);

        resourceData.unlock(); // on a fini -> on déverrouille resourceData pour pouvoir recevoir de nouvelles données!
    }
}