# Client (android)

### Fichiers .java
###### (en résumé: tous les fichiers dans app/src/main/java et ses sous-dossiers)
app/src/main/java/tm/client/MainActivity.java  
app/src/main/java/tm/client/FolderPickerActivity.java  
app/src/main/java/tm/client/ClientService.java  
app/src/main/java/tm/client/FolderPickerAdapter.java  
app/src/main/java/tm/client/ShareActivity.java  
app/src/main/java/tm/client/TMClient.java

(pour les fichiers dans app/src/main/java/tm/util, voir dépôt du serveur)

### Fichiers .xml
###### Tout ce qui touche à l'aspect graphique de l'interface
app/src/main/res/values/colors.xml  
app/src/main/res/values/strings.xml  
app/src/main/res/drawable/gradient_background.xml  
app/src/main/res/layout/textview_folder_picker.xml  
app/src/main/res/layout/activity_main.xml  
app/src/main/res/layout/activity_folder_picker.xml  
app/src/main/res/layout/activity_share.xml
